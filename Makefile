bin/many_images_processing: obj/main.o include/image_procesor.h
	mkdir -p bin
	gcc -o bin/many_images_processing obj/main.o

obj/main.o: src/main.c include/image_procesor.h
	mkdir -p obj
	gcc -c src/main.c -o obj/main.o

.PHONY: clean

clean: 
	rm obj/*.o bin/many_images_processing