#include "../include/image_procesor.h"

int main(int argc, char **argv){

    char procesor[] = "./procesador_png";

    char new_image[MAX] = {0};

    pid_t pids[MAX];

    pid_t pid;

    if(argc <= 1) 
        perror("Envie las imagenes a procesar\n");

    for(int i= 0; i<argc-1;i++){
        if((pid = fork()) == 0){
            caller_process_image(procesor,argv[i+1],(i+1),new_image);
        }else{
            pids[i] = pid;
        }
    }

    int status;

    for(int i= 0; i<argc-1;i++){
        if (waitpid(pids[i],&status,0) >= 0){
            printf("%s (archivo BW %d.png): status %d\n",argv[i+1], (i+1), WEXITSTATUS(status));
        }       
    }

}

void caller_process_image(char *procesor, char *image,int contador, char *new_image){
    snprintf(new_image, MAX, "%d.png", contador);
    char *argv2[] = {procesor,image,new_image, NULL};
    execvp(procesor,argv2);
    exit(-1);
}